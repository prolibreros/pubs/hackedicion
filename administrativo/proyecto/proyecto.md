# Hackedición. Manual introductorio a tecnologías editoriales libres

# Características

En la actualidad la industria editorial enfrenta una serie de desafíos.
Por un lado, existe la necesidad de generar múltiples formatos y ya no
solo archivos para impresión. Por el otro, se está en la búsqueda de
*la* herramienta que con un solo clic solvente esta necesidad.

*Hackedición. Manual introductorio a tecnologías editoriales libres* es
una publicación cuyo enfoque metodológico pretende dar una alternativa a
la solución de estos desafíos. En el manual se mostrará que, lo que en
apariencia es una mera cuestión técnica, es en realidad un problema
metodológico que viene arrastrándose desde hace unas décadas.

Con este manual se explicará el método de edición ramificada, el cual
consiste en generar de manera automatizada varios formatos requeridos
para la edición contemporánea a partir de una serie de archivos
iniciales conocidos como «archivos madre». Con esta metodología es
posible publicar de manera automatizada y multiformato, permitiendo a
las personas que editan dedicarse a su labor más importante: el cuidado
del texto y de la edición.

Otro desafío en los procesos editoriales son los costos cada vez más
altos para tener acceso a las tecnologías comunes para la producción de
publicaciones. Por este motivo, el enfoque pedagógico de este manual
también consiste es ser una introducción a las tecnologías para la
edición disponibles como *software* libre o de código abierto
(+++FOSS+++, por sus siglas en inglés).

Paso a paso este manual instruirá al usuario sobre la instalación y uso
de diversos programas multiplataforma que le harán notar las ventajas y
versatilidades que ofrece el uso de +++FOSS+++ y de formatos abiertos
para el desarrollo, mantenimiento y conservación a largo plazo de
proyectos editoriales. Todo esto con suplementos informativos que
contextualizarán cada una de las tecnologías empleadas.

Como la producción de una publicación es solo el primer paso. Este
manual también cubrirá una explicación de diversas licencias de uso
disponibles para los productos culturales. De las licencias Creative
Commons a la Licencia de Producción de Pares o la Licencia Editorial
Abierta y Libre ---licencia que tendrá este manual---, el usuario/editor
tendrá al alcance posibilidades de compartición más flexibles a los
modos tradicionales de gestión de derechos al mismo tiempo que garantiza
la protección de su trabajo bajo todas las formalidades de la Ley
Federal del Derecho de Autor.

Por último este manual también enseñará a sus usuarios a hacer
disponible sus publicaciones a diversas plataformas de distribución que
apuestan por una cultura libre. Por ejemplo, el usuario aprenderá a
subir documentos a Internet Archive, LibGen, Aaaaarg, Memory of the
World y Basura Fantasma.

Con estos elementos, es posible hablar que además de un manual sobre
edición, el usuario tendrá disponible un manual sobre cómo hacer para
que su computadora trabaje de la manera como quiere para obtener
productos editoriales. Este tipo de empoderamiento tecnológico es la
definición originaria del verbo «*hack*» en su acepción técnica. Por eso
esta manual conformaría lo que en el medio editorial puede conocerse
como *hackedición*.

# Objetivos

- Producir un manual para que el lector aprenda a editar textos y
  publicarlos usando +++FOSS+++ y formatos abiertos.
- Redactar una publicación que explique la pertinencia y espacio legal
  de las licencias de uso en México, así como su distribución en
  canales alternativos.
- Desarrollar la obra en diversos formatos y de manera automatizada,
  según los mismos conocimientos expuestos en la obra.
- Grabar varios videotutoriales similares a los cursos de Lynda o
  Video2Brain sobre varios contenidos explicados paso a paso en el
  manual.

# Producto cultural resultante

Como la publicación sigue el esquema de edición ramificada, es posible
obtener distintos soportes, como son:

- Publicación en +++PDF+++ para impresión.
- Publicación en +++EPUB+++ para cualquier dispositivo excepto Kindle.
- Publicación en +++MOBI+++ para Kindle.
- Publicación en +++HTML+++ para su lectura en línea.
- Publicación en +++XML+++ y +++JSON+++ para su uso en procesamiento
  de información.
- Publicación en +++MD+++ y TeX como archivos base para la edición.
- Una serie de videotutoriales ---al menos diez--- donde se explique
  paso a paso las actividades indicadas en la publicación en formato
  +++MPEG4+++ H.264.
- Una plataforma *web* para descargar el libro en diferentes formatos
  y los videotutoriales.
- Repositorios con todos los contenidos ---editables y finales--- en
  GitHub, GitLab y mi servidor.
- Copia del repositorio en Internet Archive para fines de
  conservación.
- 20 ejemplares impresos para los usos que Secretaría Cultura desee
  darles.

# Calendario y descripción de actividades

---- -------------------------------- --------------------------------------------------------------------------------------------
1    Redacción del manual             Redacción al 25% de la obra
2    Redacción del manual             Redacción al 50% de la obra
3    Redacción del manual             Redacción al 75% de la obra
4    Redacción del manual             Redacción al 100% de la obra
5    Grabación de videotutoriales     Grabación al 50% de los tutoriales
6    Grabación de videotutoriales     Grabación al 100% de los tutoriales
7    Edición del manual               Corrección y lectura de pruebas de la obra
8    Publicación del manual           Generación de formatos finales de la obra
9    Levantamiento de repositorios    Subida de todos los archivos del proyecto a GitHub, GitLab, mi servidor e Internet Archive
10   Desarrollo de plataforma *web*   Generación de un micrositio de perrotuerto.blog para uso exclusivo de este manual
11   Impresión del manual             Impresión de al menos 20 ejemplares del manual
12   Presentación del manual          Presentación del manual en espacios autogestionados en Colima y Ciudad de México
---- -------------------------------- --------------------------------------------------------------------------------------------

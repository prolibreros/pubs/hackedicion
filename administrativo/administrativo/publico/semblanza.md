Ramiro Santa Ana Anguiano (Colima, 18 de abril de 1989) es editor y
programador. En la actualidad está terminando la Maestría en Filosofía
en la UNAM con una investigación sobre crítica a la propiedad
intelectual y comenzando el proyecto _Hackedición. Manual introductorio
a tecnologías editoriales libres_ gracias al Fondo Nacional para la
Cultura y las Artes 2020. Su trabajo profesional se orienta a la
cultura, _software_ y edición libres. Esto lo ha llevado desde el
quehacer editorial tradicional como es la traducción, curaduría,
corrección y maquetación, hasta el levantamiento de plataformas _web,_
la digitalización de libros y el desarrollo de _software_ libre para la
edición. Ha impartido varios cursos, talleres o charlas sobre edición,
como en los diplomados de edición de Sexto Piso o los ofrecidos por la
FAD-UNAM, así como en _hackerspaces_ autogestionados en la Ciudad de
México y en Colima.

Filiaciones:

* Grupo de trabajo de Programando LIBREros
* Comunidad de editores libres Miau
* Miembro de Orbilibro Ediciones

Contacto:

* Mastodon: [https://mastodon.social/\@\_perroTuerto]{.underline}
* Correo: [hi\@perrotuerto.blog]{.underline}
* Sitio: [https://perrotuerto.blog]{.underline}
* Herramientas editoriales:
  [[https://pecas.perrotuerto.blog]{.underline}](https://pecas.perrotuerto.blog/)
* Taller de Edición Digital:
  [[https://ted.perrotuerto.blog]{.underline}](https://ted.perrotuerto.blog/)
* GitLab: [https://gitlab.com/NikaZhenya]{.underline}
* GitHub:
  [[https://github.com/NikaZhenya]{.underline}](https://github.com/NikaZhenya)
